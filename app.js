const {client, PREFIX, openai} = require('./config');
const {constants} = require('./constants');

client.on('ready', () => {
    logger.log('== botonios is ready! ==');
});


client.on('messageCreate', async message =>{
    if(messageIsNotReadable()) return;

    openai.getAnswer(message).then(msg => {
        reply(message.channelId, msg)
    });
});

/**
 * AI message given message to given channel
 * @param channelId
 * @param message
 */
function reply(channelId, message){
    if(message != '')
        client.channels.cache.get(channelId).send(message);
    else
        client.channels.cache.get(channelId).send("_crash_");

}

/**
 * check if message has text that could make AI crash
 * @param message
 * @returns {boolean}
 */
function messageIsNotReadable(message){
    let isNotReadable = false;

    if (message.author.bot
    && !message.guild
    && !message.content.startsWith(PREFIX)
    && message.content.startsWith('http'))
    {
        isNotReadable = true;
    }

    return isNotReadable;
}

client.login(constants.DISCORDAPP_TOKEN);
