/**
 * CONFIGURATION
 */

const fs = require('fs');
const constants  = {};

if(!fs.existsSync('./local-settings.json'))
{
    constants.DISCORDAPP_TOKEN = process.env.DISCORDAPP_TOKEN;
    constants.OPENAI_API_KEY = process.env.OPENAI_API_KEY;
}
else
{
    let settings = require("./local-settings.json");
    constants.DISCORDAPP_TOKEN = settings.discordapp.token;
    constants.OPENAI_API_KEY = settings.openai.token;
}

module.exports = { constants }
