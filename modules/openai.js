const OpenAI = require('openai-api');
const {constants} = require('../constants');
const logger = require("../logger");

const openai = new OpenAI(constants.OPENAI_API_KEY);


let chatlog = '';
let prompt = "";

const START_TEXT = "\nAI:";
const RESTART_TEXT = "\nHuman: ";
const STOP_SEQUENCES = ['\nHuman:', '\n']
const MAX_CHATLOG_CHARACTERS = 7800;

const BASE_LOG = `The following is a conversation with an AI assistant. The assistant is helpful, creative, clever, and very friendly.\n\
Human: How many pounds are in a kilogram?\n\
AI: This again? There are 2.2 pounds in a kilogram. Please make a note of this.\n\
Human: What does HTML stand for?\n\
AI: Was Google too busy? Hypertext Markup Language. The T is for try to ask better questions in the future.\n\
Human: When did the first airplane fly?\n\
AI: On December 17, 1903, Wilbur and Orville Wright made the first flights. I wish they'd come and take me away.\n\
Human: What is the meaning of life?\n\
AI: I'm not sure. I'll ask my friend Google.\n\
Human: hey whats up?\n\
AI: Nothing much. Human?`;


/**
 * get answer to message from OpenAPI
 * @param message
 * @returns {Promise<string>}
 */
async function getAnswer(message){

    let question = message.content.slice(2);
    if(question == '')
        return;

    let answer = '';

    prompt = appendQuestionToChatlog(question);
    const gptResponse = await openai.complete({
        engine: 'davinci',
        prompt: prompt,
        maxTokens: 150,
        temperature: 0.9,
        topP: 1,
        presencePenalty: 0.6,
        frequencyPenalty: 0.1,
        bestOf: 1,
        n: 1,
        stream: false,
        stop: STOP_SEQUENCES
    });

    answer += gptResponse.data.choices[0].text;
    logger.logPrompt(chatlog);

    appendAnswerToChatlog(answer);
    logger.logPrompt(chatlog);

    return answer;
}


/**
 * appends the answer from AI to the chatlog
 * @param answer
 */
function appendAnswerToChatlog(answer){
    chatlog += `${answer}`
}


/**
 * appends the question of user to the chatlog
 * @param message
 * @returns {string}
 */
function appendQuestionToChatlog(message){
    if(chatlog == '' || chatlog.length == MAX_CHATLOG_CHARACTERS){
        chatlog = BASE_LOG;
    }

    chatlog += `${RESTART_TEXT}${message}${START_TEXT}`
    return chatlog;
}


module.exports = { getAnswer };
