/**
 *  REQUIRED IMPORTS
 */

const {Client, Intents} = require('discord.js');
const openai = require('./modules/openai');

/**
 *  GLOBAL VARIABLES
 */

const PREFIX = "::";

const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.DIRECT_MESSAGES, Intents.FLAGS.GUILD_MESSAGES] });
const constants = {};


/**
 * CONSTANTS TO EXPORT
 */

module.exports = {
    PREFIX,
    client,
    openai,
    constants
}
